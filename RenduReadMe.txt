Nous avons décidé de faire une branche par features afin de pouvoir travailler chacun de son coté puis pour laisser une version stable sur la branche master.

Une fois les features faite nous les avons regroupé afin de les tester ensemble puis merge le résultat dans master afin de mettre à jour la version stable. 

Lorsque nous avons voulu récupérer les modifications il était impossible de merge le master mis à jour dans l'une de nos branches a cause d'une erreur de Lfs.

nous avons donc créé une autre branche avec les modifications que nous étions capables de récupérer (donc sans les Lfs) en ajoutant les dlls
puis nous avons fait les fix sur cette nouvelle branche pour enfin mettre à jour le master.